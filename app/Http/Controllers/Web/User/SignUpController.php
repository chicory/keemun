<?php

namespace App\Http\Controllers\Web\User;

use App\Http\Controllers\Controller;
use App\Http\Requests\Web\SignUpRequest;
use App\Services\Web\SignUpService;

class SignUpController extends Controller
{
    public function __construct(
        private SignUpService $signUp
    ) {
    }

    public function __invoke(SignUpRequest $request)
    {
        return $this->signUp->execute($request);
    }
}
