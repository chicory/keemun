<?php

namespace App\Http\Controllers\Web\User;

use App\Http\Controllers\Controller;
use App\Http\Requests\Web\AuthRequest;
use App\Services\Web\AuthService;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function __construct(
        private AuthService $auth
    ) {
    }

    /**
     * Authentication.
     *
     * @param \App\Http\Requests\Web\AuthRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function authenticate(AuthRequest $request): RedirectResponse
    {
        return $this->auth->authenticate($request);
    }

    /**
     * LogOut.
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function logout(Request $request): RedirectResponse
    {
        return $this->auth->logout($request);
    }
}
