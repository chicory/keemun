<?php

namespace App\Http\Controllers\Api\Discovery;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\WebFingerRequest;
use App\Services\Discovery\WebFingerService;

class WebFingerController extends Controller
{
    public function __construct(
        private WebFingerService $webFinger
    ) {
    }

    /**
     * WebFinger entry point.
     *
     * @return array
     */
    public function __invoke(WebFingerRequest $request): array
    {
        return $this->webFinger->resolve($request);
    }
}
