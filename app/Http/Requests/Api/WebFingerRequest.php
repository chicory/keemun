<?php

namespace App\Http\Requests\Api;

use Illuminate\Foundation\Http\FormRequest;

class WebFingerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'resource' => ['required']
        ];
    }

    /**
     * Validation errors.
     *
     * @return array<string>
     */
    public function messages()
    {
        return [
            'resource.required' => "Incorrect query, 'resource' is required!",
        ];
    }
}
