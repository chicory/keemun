<?php

namespace App\Http\Requests\Web;

use Illuminate\Foundation\Http\FormRequest;

class AuthRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'email' => ['required', 'email', 'max:128', 'min:8'],
            'password' => ['required', 'max:128', 'min:8'],
        ];
    }

    /**
     * Validation errors.
     *
     * @return array<string>
     */
    public function messages()
    {
        return [
            'email.required' => __('error.email_required'),
            'email.email' => __('error.email_invalid'),
            'email.min' => __('error.email_min'),
            'email.max' => __('error.email_max'),
            'password.required' => __('error.password_required'),
            'password.min' => __('error.password_min'),
            'password.max' => __('error.password_max'),
        ];
    }
}
