<?php

namespace App\Http\Requests\Web;

use Illuminate\Foundation\Http\FormRequest;

class SignUpRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'name' => ['required', 'regex:/^[a-z0-9]{3,32}$/', 'unique:users,name'],
            'email'=> ['required', 'max:128', 'min:8', 'email', 'unique:users,email'],
            'password' => ['required', 'max:128', 'min:8'],
            'accept' => ['accepted'],
        ];
    }

    /**
     * Validation errors.
     *
     * @return array<string>
     */
    public function messages()
    {
        return [
            'name.required' => __('error.name_required'),
            'name.unique' => __('error.name_not_unique'),
            'name.regex' => __('error.name_invalid'),
            'email.required' => __('error.email_required'),
            'email.unique' => __('error.email_not_unique'),
            'email.email' => __('error.email_invalid'),
            'email.min' => __('error.email_min'),
            'email.max' => __('error.email_max'),
            'password.required' => __('error.password_required'),
            'password.min' => __('error.password_min'),
            'password.max' => __('error.password_max'),
            'accept.accepted' => __('error.terms'),
        ];
    }
}
