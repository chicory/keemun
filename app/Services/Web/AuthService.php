<?php

namespace App\Services\Web;

use App\Http\Requests\Web\AuthRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

class AuthService
{
    /**
     * Authentication.
     *
     * @param \App\Http\Requests\Web\AuthRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function authenticate(AuthRequest $request): RedirectResponse
    {
        if (Auth::attempt(
            $request->safe()->only(['email', 'password']),
            $request->has('remember')
        )) {
            $request->session()->regenerate();
            return redirect()->route('account');
        }
        return back()->withErrors([
            'auth_failed' => __('error.auth_failed'),
        ]);
    }

    /**
     * LogOut.
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function logout(Request $request): RedirectResponse
    {
        Auth::logout();
        $request->session()->invalidate();
        $request->session()->regenerateToken();
        return redirect()->route('index');
    }
}
