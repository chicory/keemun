<?php

namespace App\Services\Web;

use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Http\Requests\Web\SignUpRequest;
use App\Models\User;

class SignUpService
{
    /**
     * User registration.
     *
     * @param \App\Http\Requests\Web\SignUpRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function execute(SignUpRequest $request): RedirectResponse
    {
        if (!config('app.signup')) {
            return back()->withErrors([
                'signup_failed' => __('error.signup_disallowed'),
            ]);
        }
        Auth::login($this->createUser($request));
        return redirect()->route('account');
    }

    /**
     * Creates new user from request.
     *
     * @param \App\Http\Requests\Web\SignUpRequest $request
     * @return \App\Models\User
     */
    private function createUser(SignUpRequest $request): User
    {
        $user = new User([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
        ]);
        $user->save();
        return $user;
    }
}
