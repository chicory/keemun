<?php

namespace App\Services\Discovery;

use App\Http\Requests\Api\WebFingerRequest;

class WebFingerService
{
    /** WebFinger path */
    public const PATH = '/.well-known/webfinger';

    /**
     * Request resolver.
     *
     * @return array
     */
    public function resolve(WebFingerRequest $request): array
    {
        return [];
    }
}
