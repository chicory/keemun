<?php

namespace App\Config;

/**
 * Application default config values.
 *
 * @var APP_VERSION Application Verion.
 * @var APP_NAME Application Name.
 * @var APP_ENV Application Environment.
 * @var APP_DEBUG Application Debug Mode.
 * @var APP_URL Application URL
 * @var APP_TIMEZONE Application Environment
 * @var APP_LOCALE Application Locale Configuration
 * @var APP_FALLBACK_LOCALE Application Fallback Locale
 * @var APP_DEFAULT_THEME Application Default Theme
 * @var APP_ALLOW_SIGNUP Application User registration is open
 */
class AppConfigDefaults
{
    /** Application Verion */
    public const APP_VERSION = '(dev)';
    /** Application Name. */
    public const APP_NAME = 'Keemun';
    /** Application Environment. */
    public const APP_ENV = 'production';
    /** Application Debug Mode */
    public const APP_DEBUG = false;
    /** Application URL */
    public const APP_URL = 'UTC';
    /** Application Timezone */
    public const APP_TIMEZONE = 'UTC';
    /** Application Locale Configuration */
    public const APP_LOCALE = 'en';
    /** Application Fallback Locale */
    public const APP_FALLBACK_LOCALE = 'en';
    /** Application Default Theme */
    public const APP_DEFAULT_THEME = 'default';
    /** Application User registration is open */
    public const APP_ALLOW_SIGNUP = true;
}
