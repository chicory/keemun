<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/** Public routes */
Route::group(['name' => 'public'], function () {
    Route::get('/', \App\Http\Controllers\Web\IndexController::class)->name('index');
    Route::view('/terms', 'terms')->name('terms');
    Route::view('/about', 'about')->name('about');
});

/** User routes */
Route::group(['name' => 'user'], function () {
    /** */
    Route::group(['middleware' => 'auth'], function () {
        Route::view('/account', 'account.account')->name('account');
        Route::get('/logout', ['\App\Http\Controllers\Web\User\AuthController', 'logout'])->name('logout');
    });
    /** */
    Route::group(['middleware' => 'guest'], function () {
        Route::view('/login', 'account.login')->name('login');
        Route::view('/password_recovery', 'account.password_recovery')->name('password_recovery');
        Route::view('/create_account', 'account.create_account')->name('create_account');
        Route::post('/authenticate', ['\App\Http\Controllers\Web\User\AuthController', 'authenticate'])->name('authenticate');
        Route::post('/signup', \App\Http\Controllers\Web\User\SignUpController::class)->name('signup');
    });
});

/** Test routes */
if (config('app.debug')) {
    Route::view('/template', 'template');
}
