@extends('templates.layout')

@section('title') Default template demo @endsection
@section('description') Default template demo. @endsection
@section('keywords') blade, html, css @endsection

@section('content')
<div class="body-container">
    <!-- Main container -->
    <section class="main">
        <!-- Section -->
        <div class="title">
            <div class="title_tab">
                Section title
            </div>
            <div class="title_etc">            
                <a href="#">SubLink</a>
            </div>     
        </div> 
        <div class="clear"></div>
        <div class="content">
            <h1><a href="#" class="post_title">H1 Title</a></h1>
            <p>
                Some paragraph.<br>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
                Quisque sodales leo tellus. Ut pharetra iaculis elit sit amet molestie. 
                Duis ornare sed dui non tempor. 
                Vestibulum pretium imperdiet rhoncus. In et ipsum neque.
            </p>
            <h2>H2 title</h2>
            <ul>
                <li><a href="#"> Item Link</a></li>
                <li>Item</li>
            </ul>
            <blockquote>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
                Quisque sodales leo tellus. Ut pharetra iaculis elit sit amet molestie. 
                Duis ornare sed dui non tempor. 
                Vestibulum pretium imperdiet rhoncus. In et ipsum neque.
            </blockquote>
            <pre><code>#!/bin/bash</code></pre>
            <!-- Form -->
            <textarea name="" id="" rows="3" class="editor">Text</textarea><br>
            <select name="" id="" class="editor">
                <option value="" class="editor">Option 1</option>
                <option value="" class="editor">Option 2</option>
            </select><br>
            <input type="text" class="editor"><br>
            <button class="button">Button</button><br>
            <!-- Messages -->
            <div class="error">Error!</div>
            <div class="success">Success!</div>
        </div>
    </section>
    <!-- Sidebar -->
    <section class="sidebar">
        <div class="title">
            <div class="title_tab">
                Title
            </div>
            <div class="title_etc">
                <a href="#">SubLink</a>          
            </div>     
        </div>
        <div class="clear"></div>
        <div class="categories">
            <a href="#">Item 1</a>    
            <a href="#">Item 2</a>
        </div>
    </section>
  </div>
  <div class="clear"></div>
@endsection