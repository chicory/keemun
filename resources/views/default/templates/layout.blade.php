<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="{{ url('assets/default/main.css') }}">
    <link rel="shortcut icon" href="{{ url('favicon.png') }}" type="image/png">
    <meta name="description" content="@yield('description')">
    <meta name="keywords" content="@yield('keywords')">
    <title>@yield('title')</title>
</head>
<body>
    @include('templates.header')
    <div class="body-container" style="width: 100%">
    @yield('content')
    </div>
    @include('templates.footer')
</body>
</html>
<!-- -->