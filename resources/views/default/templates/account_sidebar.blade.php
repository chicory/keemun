<section class="sidebar">
    <div class="title">
        <div class="title_tab">
            {{ __('account.actions') }}
        </div>    
    </div>
    <div class="clear"></div>
    <div class="categories">
        <a href="{{ route('logout') }}">{{ __('account.logout') }}</a>
    </div>
</section>