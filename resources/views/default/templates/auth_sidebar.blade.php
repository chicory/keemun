@php
    $routeName = Request::route()->getName();
@endphp
<section class="sidebar">
    <div class="title">
        <div class="title_tab">
            {{ __('account.actions') }}
        </div>    
    </div>
    <div class="clear"></div>
    <div class="categories">
        @if($routeName != 'login') <a href="{{ route('login') }}">{{ __('account.login') }}</a> @endif     
        @if($routeName != 'create_account') <a href="{{ route('create_account') }}">{{ __('account.create_account') }}</a> @endif     
        @if($routeName != 'password_recovery') <a href="{{ route('password_recovery') }}">{{ __('account.password_recovery') }}</a> @endif
    </div>
</section>