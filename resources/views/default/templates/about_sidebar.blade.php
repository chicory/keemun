<section class="sidebar">
    <div class="title">
        <div class="title_tab">
            {{ __('title.about') }}
        </div>    
    </div>
    <div class="clear"></div>
    <div class="categories">
        <a href="{{ route('terms') }}">{{ __('title.terms') }}</a>
        <a href="{{ route('about') }}">{{ __('title.about') }}</a>
    </div>
</section>