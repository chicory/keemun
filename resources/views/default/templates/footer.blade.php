</div>
<div class="clear"></div>
<footer>
    <div class="body-container">
        <div class="footer_copy">
            &copy; {{ config('app.name') }}<br>
            <a href="{{ route('terms') }}">{{ __('title.terms') }}</a><br>
            <a href="{{ route('about') }}">{{ __('title.about') }}</a><br>    
        </div>      
        <div class="footer_text">  
            Powered by <a href="https://codeberg.org/chicory/keemun/src/branch/dev">Keemun {{ config('app.version')}}</a>
        </div>
      </div>
    </footer>
</footer> 