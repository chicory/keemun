<header id="header">
    <div class="body-container">
        <div class="logo">
            <a href="{{ route('index') }}">{{ config('app.name') }}</a>
        </div>
        <input class="header-menu" type="checkbox" id="header-menu"/>
        <label class="menu-btn" for="header-menu"><span class="menu-btn-line"></span></label>
        <nav>
            <a href="#" class="nav">Timeline</a>
            <a href="#" class="nav">Users</a>
            <a href="{{ route('account') }}" class="nav">Account</a> 
        </nav>
    </div>
</header>
<div class="clear"></div>
<div class="body-container">