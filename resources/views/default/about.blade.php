@extends('templates.layout')

@section('title') {{ __('title.about') }} | {{ config('app.name') }} @endsection

@section('content')
    <section class="main">
        <div class="title">
            <div class="title_tab">
                {{ __('title.about') }}
            </div>   
        </div> 
        <div class="clear"></div>
        <div class="content">
            @php echo file_get_contents(base_path('static/about.html')) @endphp
        </div>
    </section>
    @include('templates.about_sidebar')
@endsection