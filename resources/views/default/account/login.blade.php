@extends('templates.layout')

@section('title') {{ __('title.login') }} | {{ config('app.name') }} @endsection

@section('content')
    <section class="main">
        <div class="title">
            <div class="title_tab">
                {{ __('title.login') }}
            </div>   
        </div> 
        <div class="clear"></div>
        <div class="content">
            @include('templates.error')
            <div class="center">
                <form action="{{ route('authenticate') }}" method="post">
                    @csrf
                    <input type="email" name="email" placeholder="{{ __('account.email') }}"><br>
                    <input type="password" name="password" placeholder="{{ __('account.password') }}"><br>
                    <input type="checkbox" name="remember" id="remember"> <label for="remember">{{ __('account.remember') }}</label><br> 
                    <input type="submit" value="{{ __('account.login') }}" class="button">
                </form>
            </div>
        </div>
    </section>
    @include('templates.auth_sidebar')
@endsection