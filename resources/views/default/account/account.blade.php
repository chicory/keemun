@extends('templates.layout')

@section('title') {{ __('title.account') }} | {{ config('app.name') }} @endsection

@section('content')
    <section class="main">
        Hello {{ auth()->user()->name }} !
    </section>
   @include('templates.account_sidebar')
@endsection