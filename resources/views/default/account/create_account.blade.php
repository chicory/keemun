@extends('templates.layout')

@section('title') {{ __('title.create_account') }} | {{ config('app.name') }} @endsection

@section('content')
    <section class="main">
        <div class="title">
            <div class="title_tab">
                {{ __('title.create_account') }}
            </div>   
        </div> 
        <div class="clear"></div>
        <div class="content">
            @include('templates.error')
            @if(config('app.signup')) 
                <div class="center">
                    <form action="{{ route('signup') }}" method="post">
                        @csrf
                        <input type="text" name="name" placeholder="{{ __('account.name') }}"><br>
                        <input type="email" name="email" placeholder="{{ __('account.email') }}"><br>
                        <input type="password" name="password" placeholder="{{ __('account.password') }}"><br>
                        <input type="checkbox" name="accept" id="accept"> 
                        <label for="accept">
                            {{ __('account.accept_terms') }} <a href="{{ route('terms') }}">{{ __('title.terms')}}</a>
                        </label><br> 
                        <input type="submit" value="{{ __('account.create_account') }}" class="button">
                    </form>
                </div>
            @else 
                <div class="error">{{ __('error.signup_disallowed') }}</div>
            @endif
        </div>
    </section>
    @include('templates.auth_sidebar')
@endsection