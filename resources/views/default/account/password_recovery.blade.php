@extends('templates.layout')

@section('title') {{ __('title.password_recovery') }} | {{ config('app.name') }} @endsection

@section('content')
    <section class="main">
        <div class="title">
            <div class="title_tab">
                {{ __('title.password_recovery') }}
            </div>   
        </div> 
        <div class="clear"></div>
        <div class="content">
            <div class="center">
                <form action="" method="post">
                    @csrf
                    <input type="email" name="email" placeholder="{{ __('account.email') }}"><br>
                    <input type="submit" value="{{ __('account.password_recovery') }}" class="button">
                </form>
            </div>
        </div>
    </section>
    @include('templates.auth_sidebar')
@endsection