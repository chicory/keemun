<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Account Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'name' => 'Username',
    'email' => 'E-mail',
    'password' => 'Password',
    'remember' => 'Remember me',
    'login' => 'LogIn',
    'logout' => 'LogOut',
    'actions' => 'Actions',
    'password_recovery' => 'Restore password',
    'create_account' => 'Create an account',
    'accept_terms' => 'I accept',
];
