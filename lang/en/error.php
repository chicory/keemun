<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Errors Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'auth_failed' => 'Invalid credentials',
    'signup_disallowed' => 'Registration for new users is closed, please check back later',

    'name_required' => 'Username field is required',
    'name_not_unique' => 'A user with the same username already exists',
    'name_invalid' => 'Username must be between 3 and 32 in length and consist of a-z 0-9',
    'email_required' => 'E-mail field is required',
    'email_not_unique' => 'A user with the same E-mail already exists',
    'email_invalid' => 'E-mail is not valid',
    'email_min' => 'E-mail must be minimum :min characters long',
    'email_max' => 'E-mail must be maximum :max characters long',
    'password_required' => 'Password field is required',
    'password_min' => 'Password must be minimum :min characters long',
    'password_max' => 'Password must be maximum :max characters long',
    'terms' => 'Terms of service not accepted',
];
