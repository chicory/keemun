<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Page Titles Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'account' => 'My account',
    'login' => 'LogIn',
    'create_account' => 'Account creation',
    'password_recovery' => 'Password recovery',
    'terms' => 'Terms of service',
    'about' => 'About service',

];
