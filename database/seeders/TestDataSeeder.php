<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class TestDataSeeder extends Seeder
{
    /**
     * Run the database test data seeders.
     *
     * @return void
     */
    public function run(): void
    {
        $this->call([
            TestUsersSeeder::class,
        ]);
    }
}
